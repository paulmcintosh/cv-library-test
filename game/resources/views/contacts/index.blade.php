<!-- header -->
@include('contacts.partials.header')
<!-- header -->
        
        <!--h1 -->
        <h1>Contacts</h1>
        <!-- h1 -->
        
        <!-- h2 -->
        <h2>View Contact Details</h2>
        <!-- h2 -->
        
        <!-- search -->
        <div class="row">
            @include('contacts.partials.searchbar')
        </div>
        <!-- search -->
        
        <!-- records -->
        <div id="records_data">
        @include('contacts.partials.contact')
        </div>
        <!-- records -->
    


<!-- footer -->
@include('contacts.partials.header')
<!-- footer -->