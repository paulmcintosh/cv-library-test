<!-- header -->
@include('records.partials.header')
<!-- header -->


<h1>{{ $h1 }}</h1>



<!-- results -->
<table class="table">
	
	<!-- tbody -->
	<tbody>
		@include('records.partials.results')
	</tbody>
	<!-- tbody -->
	
</table>
<!-- results -->

<!-- footer -->
@include('records.partials.footer')
<!-- footer -->