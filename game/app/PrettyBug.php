<?php

namespace App;

class PrettyBug {
    
    public function __construct($bug){
        
        $this->render(array('type'=>gettype($bug)));

        $this->render($bug);

        $this->detect($bug);
        
    }
    
    public function detect($bug){
        
        if(gettype($bug) == 'class'){
            
            $this->render(get_class_methods($bug));
            
        } else if(gettype($bug) == 'object'){
            
            $this->render(get_object_vars($bug));
            
        }
    }
    
    public function render($bug){
        
        if(gettype($bug) == 'string'){
            
            echo $bug . "</br>";
            
        } else {

            echo "<pre>";
            print_r($bug);
            echo "</pre>";
         
        }
   
    }
}

?>